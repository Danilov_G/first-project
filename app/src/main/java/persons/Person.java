package persons;

public class Person {

    protected String name;
    protected Criterion arr;

    Person(String name, Criterion arr){
        this.name = name;
        this.arr = arr;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Criterion getArrOfCriterion(){
        return this.arr;
    }

    public void setArrOfCriterion(Criterion arr){
        this.arr = arr;
    }

}
