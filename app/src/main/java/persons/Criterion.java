package persons;

public class Criterion {
    private double authority;
    private double charisma;
    private double diplomacy;
    final static double LIMIT = 100;
    final static double VALUEOFCRITERIONS = 3;
    final static double COEFFISIENTSUM = 6;
    final static double MINVALUE = 0;

//    persons.Criterion(double authority, double charisma, double diplomacy){
//        this.authority = authority;
//        this.charisma = charisma;
//        this.diplomacy = diplomacy;
//    }

    public double getAuthority(){
        return this.authority;
    }

    public double getCharisma(){
        return this.charisma;
    }

    public double getDiplomacy(){
        return this.diplomacy;
    }

    public static double getLIMIT() {
        return LIMIT;
    }

    public static double getCOEFFISIENTSUM() {
        return COEFFISIENTSUM;
    }

    public static double getVALUEOFCRITERIONS() {
        return VALUEOFCRITERIONS;
    }

    public void setAuthority(double authority) {
        if (authority <= LIMIT && authority >= MINVALUE) {
            this.authority = authority;
        } else {
            System.err.println("Введите число от "+MINVALUE+ "до " +LIMIT);
        }
    }

    public void setCharisma(double charisma) {
        if (charisma <= LIMIT && charisma >= MINVALUE) {
            this.charisma = charisma;
        } else {
            System.err.println("Введите число от "+MINVALUE+ "до " +LIMIT);
        }
    }

    public void setDiplomacy(double diplomacy) {
        if (diplomacy <= LIMIT && diplomacy >= MINVALUE) {
            this.diplomacy = diplomacy;
        } else {
            System.err.println("Введите число от "+MINVALUE+ "до " +LIMIT);
        }
    }

}
