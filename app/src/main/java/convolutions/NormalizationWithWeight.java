package convolutions;

import persons.*;

public class NormalizationWithWeight extends ConvolutionWithWeights {
    public static double conv(Person person){
        return ConvolutionWithWeights.conv(person) / (Criterion.getLIMIT() *  Criterion.getCOEFFISIENTSUM());
    }
}
