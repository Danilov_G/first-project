package convolutions;

import persons.*;

public class NormalizationWithoutWeight extends Convolution{
    public static double conv(Person person){
        return Convolution.conv(person) / (Criterion.getLIMIT() * Criterion.getVALUEOFCRITERIONS());
    }
}
