package convolutions;

import persons.*;

public class ConvolutionWithWeights extends Convolution {
    public static double conv(Person person) {
        double sum = 0;
        if (person instanceof Writer) {
            sum += person.getArrOfCriterion().getCharisma() * 3;
            sum += person.getArrOfCriterion().getDiplomacy() * 2;
            sum += person.getArrOfCriterion().getAuthority() * 1;
        }
        if (person instanceof Politic) {
            sum += person.getArrOfCriterion().getCharisma() * 3;
            sum += person.getArrOfCriterion().getDiplomacy() * 1.5;
            sum += person.getArrOfCriterion().getAuthority() * 1.5;
        }
        if (person instanceof General) {
            sum += person.getArrOfCriterion().getCharisma() * 3;
            sum += person.getArrOfCriterion().getDiplomacy() * 1;
            sum += person.getArrOfCriterion().getAuthority() * 2;
        }
        return sum;
    }
}
