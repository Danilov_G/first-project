package convolutions;

import persons.*;

public class Convolution {
    public static double conv(Person person){
        double sum = 0;
        sum += person.getArrOfCriterion().getCharisma();
        sum += person.getArrOfCriterion().getDiplomacy();
        sum += person.getArrOfCriterion().getAuthority();
        return sum;
    }
}
