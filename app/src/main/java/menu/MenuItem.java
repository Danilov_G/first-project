package menu;

import actions.Action;

import java.util.ArrayList;

public class MenuItem{
    ArrayList<MenuItem> children = new ArrayList<MenuItem>();
    MenuItem parent;
    ArrayList<Action> actions = new ArrayList<Action>();
    String str;

    public MenuItem(String str) {
        this.str = str;
    }

    public void setAction(Action action){
        this.actions.add(action);
    }


    public void connection(MenuItem nextLevel){
        this.children.add(nextLevel);
        nextLevel.parent = this;
    }

    public void printStr(){
        System.out.println();
        System.out.println("Выберетe, что хотите сделать:");

        if (this.parent == null) System.out.println("0 Выйти");
        else System.out.println("0 Вернутбся на уровень назад ");
        int k = 1;

        for (int i = 0; i < this.actions.size(); i++) {
            System.out.println(k + " " + this.actions.get(i).getStr());
            k++;
        }

        for (int i = 0; i < this.children.size(); i++) {
            System.out.println(k + " " + this.children.get(i).str);
            k++;
        }

        System.out.println();
    }

}
