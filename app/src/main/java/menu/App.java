package menu;

import actions.*;
import persons.*;

import java.util.Scanner;


public class App {

//    public static menu.MenuItem buildMenu() throws Exception {
//        menu.MenuItem begin = new menu.MenuItem("Assessment");
//        Actions.Action one = new ActionOne("Ввод данных");
//        begin.setAction(one);
//        Actions.Action two = new ActionTwo("Посчитать");
//        begin.setAction(two);
//        return begin;
//    }

    public static void main(String[] args) throws Exception {
        MenuItem begin = new MenuItem("Assessment");
        Action one = new ActionModifyPerson("Ввод данных");
        begin.setAction(one);
        Action two = new ActionCompute("Посчитать");
        begin.setAction(two);
        Scanner in = new Scanner(System.in);
        int userResponse;
        try {
            do{
                Person person = one.getPerson();
                two.setPerson(person);
                begin.printStr();
                userResponse = in.nextInt();
                System.out.println();


                if(userResponse > begin.children.size() && userResponse <= begin.actions.size() + begin.children.size()){
                    begin.actions.get(userResponse - begin.children.size() - 1).act();
                }
                if(userResponse > 0 && userResponse <= begin.children.size()){
                    begin = begin.children.get(userResponse - 1);
                }
            } while (begin.parent != null || userResponse != 0);

        } catch (Exception e) {
            System.err.println("Некорректный тип данных");
        }
    }
}

