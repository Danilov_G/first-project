package actions;


import convolutions.*;
import menu.AppException;
import persons.*;

public class ActionCompute extends Action {


    public ActionCompute(String str) {
        super(str);
    }


    public Person act() throws Exception {
        countPerson();
        return null;
    }


    public void countPerson() throws Exception {
        System.out.print("Convolutions.Convolution: ");
        int convol = checkingInt("1.Sum  2.With weight  3.Normalized  4.Norm with weight");
        if (convol <= 0 || convol > 4){
            throw new AppException();
        }
        try {
            if (convol == 1) {
                System.out.println(Convolution.conv(person));
            }
            if (convol == 2) {
                System.out.println(ConvolutionWithWeights.conv(person));
            }
            if (convol == 3) {
                System.out.println(NormalizationWithoutWeight.conv(person));
            }
            if (convol == 4) {
                System.out.println(NormalizationWithWeight.conv(person));
            }
        } catch (NullPointerException e){
            System.err.println("Вы не ввели данные");
        }

    }
}
