package actions;


import menu.AppException;
import persons.*;

import java.util.Scanner;

public class ActionModifyPerson extends Action {


    public ActionModifyPerson(String str) {
        super(str);
    }

    public Person act() throws Exception {
        return inputPerson();
    }


    public Person inputPerson() throws Exception {


        Scanner sc = new Scanner(System.in);
        Criterion crit = new Criterion();

        System.out.print("Name: ");
        String name = sc.nextLine();


        double auth = checkingInt("Auth: ");
        crit.setAuthority(auth);


        double charis = checkingInt("Charisma: ");
        crit.setCharisma(charis);


        double dip = checkingInt("Dip: ");
        crit.setDiplomacy(dip);


        int pers = checkingInt("1.Politic  2.General  3.Writer");
        if (pers <= 0 || pers > 3) {
            throw new AppException();
        }

        switch(pers) {
            case 1:
                person = new Politic(name, crit);
                break;
            case 2:
                person = new General(name, crit);
                break;
            case 3:
                person = new Writer(name, crit);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + pers);
        }
        return person;
    }


}
