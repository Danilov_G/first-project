package actions;

import persons.*;

import java.util.Scanner;

public abstract class Action {
    public String str;
    public Person person;

    public Action(String str){
        this.str = str;
    }

    public String getStr(){
        return this.str;
    }

    public Person getPerson() {return this.person;}

    public void setPerson(Person person){
        this.person = person;
    }

    public abstract Person act() throws Exception;

    public Integer checkingInt(String str) throws Exception{
        Scanner in = new Scanner(System.in);
        boolean flag = true;
        while (flag){
            System.out.println(str);
            String s = in.nextLine();
            int a;
            try{
                a = Integer.parseInt(s);
                System.out.println();
                return a;
            }catch(Exception e){
                System.err.println("Неккоректный тип данных, введите еще раз");
            }
        }
        return null;
    }

}
